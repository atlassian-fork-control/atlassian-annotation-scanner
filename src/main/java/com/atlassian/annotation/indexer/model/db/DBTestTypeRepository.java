package com.atlassian.annotation.indexer.model.db;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface DBTestTypeRepository extends CrudRepository<DBTestType, Long>{

    DBTestType findByName(String name);

    @Query("SELECT dbTestType.name FROM DBTestType dbTestType")
    List<String> getTestsTypes();
}
