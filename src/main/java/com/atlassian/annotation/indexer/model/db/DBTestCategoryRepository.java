package com.atlassian.annotation.indexer.model.db;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface DBTestCategoryRepository extends CrudRepository<DBTestCategory, Long> {

    DBTestCategory findByName(String category);

    @Query("SELECT dbTestCategory.name FROM DBTestCategory dbTestCategory")
    List<String> getTestsCategories();
}


