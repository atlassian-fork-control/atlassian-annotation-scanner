package com.atlassian.annotation.indexer.model.db;


import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "category")
public class DBTestCategory {

    @Id
    @Column(name = "category_id")
    @GeneratedValue(strategy= GenerationType.AUTO)
    long id;

    @Column(name = "category_name")
    String name;

    @ManyToMany(mappedBy = "categories")
    List<DBTest> tests;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<DBTest> getTests() {
        return tests;
    }

    public void setTests(List<DBTest> tests) {
        this.tests = tests;
    }
}
