package com.atlassian.annotation.indexer.model.tests;

/**
 * TODO: Document this class / interface here
 *
 * @since v6.3
 */
public class TotalRow {
    private String entry;
    private Long total;

    public TotalRow(String entry, Long total){
        this.entry = entry;
        this.total = total;
    }

    public String getEntry() {
        return entry;
    }

    public void setEntry(String entry) {
        this.entry = entry;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }
}
