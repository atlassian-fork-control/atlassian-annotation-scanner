package com.atlassian.annotation.indexer.model.tests;

import java.util.ArrayList;
import java.util.List;

public class Tests {

    List<Test> tests = new ArrayList<Test>();
    long startAt;
    long total;

    public List<Test> getTests() {
        return tests;
    }

    public void setTests(List<Test> tests) {
        this.tests = tests;
    }

    public long getStartAt() {
        return startAt;
    }

    public void setStartAt(long startAt) {
        this.startAt = startAt;
    }

    public long getTotal() {
        return tests.size();
    }

}
