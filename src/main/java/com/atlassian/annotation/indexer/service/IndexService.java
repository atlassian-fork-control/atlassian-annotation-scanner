package com.atlassian.annotation.indexer.service;

import com.atlassian.annotation.indexer.model.atr.Gavs;
import com.atlassian.annotation.indexer.model.tests.Tests;

import org.jboss.jandex.Index;

import java.io.InputStream;
import java.util.Map;


public interface IndexService {

    void indexTests(Gavs gavs, String testType);

    void indexTests(InputStream jarInputStream, String testType, String artifactId);

    Tests getTestByCategoriesByJar(Map<String, Index> indexes);

    Index indexJar(InputStream jarInputStream);

}
