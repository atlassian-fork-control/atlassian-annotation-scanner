package com.atlassian.annotation.indexer.service;

import com.atlassian.annotation.indexer.model.db.*;
import com.atlassian.annotation.indexer.model.tests.Test;
import com.atlassian.annotation.indexer.model.tests.Tests;
import com.atlassian.annotation.indexer.model.tests.TotalRow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class DatabaseServiceImpl implements DatabaseService {

    private static final Logger log = LoggerFactory.getLogger(DatabaseServiceImpl.class);

    @Autowired
    DBTestRepository dbTestRepository;

    @Autowired
    DBTestCategoryRepository dbTestCategoryRepository;

    @Autowired
    DBTestTypeRepository dbTestTypeRepository;

    @Override
    public void addTests(Tests tests, String testType){

        log.info(String.format("About to save %s tests in db", tests.getTests().size()));

        if (dbTestTypeRepository.findByName(testType) == null) {
            saveTests(tests,testType);
        } else {
            deleteTests(testType);
            saveTests(tests,testType);
        }

        log.info(String.format("Saved %s tests in db", tests.getTests().size()));

    }

    @Override
    public List<TotalRow> getTestCountByType() { return listToMap(dbTestRepository.getTestCountByType()); }

    @Override
    public List<TotalRow> getTestCountByCategory() { return listToMap(dbTestRepository.getTestCountByCategory());}

    @Override
    public List<TotalRow> getTestCountByCategory(String search) { return listToMap(dbTestRepository.getTestCountByCategory(search.toUpperCase()));}

    @Override
    public Tests getTestsByType(String testType) { return dbTestToTest(dbTestRepository.getTestsByType(testType));}

    @Override
    public Tests getTestsByCategory(String category) { return dbTestToTest(dbTestRepository.getTestsByCategory(category));}

    @Override
    public Tests getTestsByType(String testType, String search) { return dbTestToTest(dbTestRepository.getTestsByType(testType, search.toUpperCase()));}

    @Override
    public Tests getTestsByCategory(String category, String search) { return dbTestToTest(dbTestRepository.getTestsByCategory(category, search.toUpperCase()));}

    @Override
    public Tests getAllTests() {
        return dbTestToTest(dbTestRepository.findAll());
    }

    @Override
    public List<String> getTestCategories() { return dbTestCategoryRepository.getTestsCategories(); }

    @Override
    public List<String> getTestTypes() { return dbTestTypeRepository.getTestsTypes(); }

    private Tests dbTestToTest(Iterable<DBTest> dbTests) {
        List<Test> testList = new ArrayList<>();
        for (Iterator<DBTest> dbTestIterator = dbTests.iterator(); dbTestIterator.hasNext(); ) {
            DBTest dbTest = dbTestIterator.next();
            Test test = new Test(dbTest.getId(), dbTest.getJar(), dbTest.getName(), dbTest.getTestType().getName());
            Set<String> categories = new HashSet<>();
            for (DBTestCategory dbTestCategory: dbTest.getCategories()) {
                categories.add(dbTestCategory.getName());
            }
            test.setCategories(categories);
            test.setTestPackage(dbTest.getTestPackage());
            test.setTestClass(dbTest.getTestClass());
            testList.add(test);

            String repoUrltemplate = null;
            if (test.getTestType().indexOf("func") >= 0) {
                repoUrltemplate = "https://stash.atlassian.com/projects/JIRACLOUD/repos/jira/browse/jira-functional-tests/jira-func-tests/src/main/java/%s/%s.java";
            }
            if (test.getTestType().indexOf("webdriver") >= 0) {
                repoUrltemplate = "https://stash.atlassian.com/projects/JIRACLOUD/repos/jira/browse/jira-functional-tests/jira-webdriver-tests/src/main/java/%s/%s.java";
            }
            if (repoUrltemplate != null) {
                test.setSourceUrl(
                        String.format(
                                repoUrltemplate,
                                test.getTestPackage().replace('.', '/'),
                                test.getTestClass())
                );
            }

        }
        Tests tests = new Tests();
        tests.setTests(testList);

        return tests;
    }

    private List<TotalRow> listToMap(List resultList) {
        List<TotalRow> totalRows = new ArrayList<>();
        for (int i = 0; i < resultList.size(); i++) {
            Object[] item = (Object[])resultList.get(i);
            totalRows.add(new TotalRow((String)(item[0]),(Long)item[1]));
        }
        return totalRows;
    }

    private void saveTests(Tests tests, String testType){

        int count = 0;

        for (Test test:tests.getTests()) {

            DBTest dbTest = new DBTest();
            dbTest.setName(test.getName());
            dbTest.setJar(test.getJar());
            dbTest.setTestClass(test.getTestClass());
            dbTest.setTestPackage(test.getTestPackage());
            DBTestType dbTestType = dbTestTypeRepository.findByName(testType);
            if (dbTestType == null) {
                dbTestType = new DBTestType();
                dbTestType.setName(testType);
            }
            dbTest.setTestType(dbTestType);

            Set<DBTestCategory> categories = new HashSet<>();
            for (String category: test.getCategories()) {
                DBTestCategory dbTestCategory = dbTestCategoryRepository.findByName(category);
                if (dbTestCategory == null) {
                    dbTestCategory = new DBTestCategory();
                    dbTestCategory.setName(category);
                }
                categories.add(dbTestCategory);
            }
            dbTest.setCategories(categories);
            dbTestRepository.save(dbTest);

            if (++count % 100 == 0) {
                log.info(String.format("Suite %s saved %s/%s in db", testType, count, tests.getTests().size()));
            }
        }
        log.info(String.format("Suite %s saved %s/%s in db", testType, count, tests.getTests().size()));
    }

    private void deleteTests(String testType){
        List<DBTest> testsToBeDeleted = dbTestRepository.getTestsByType(testType);
        dbTestRepository.delete(testsToBeDeleted);
    }

}
