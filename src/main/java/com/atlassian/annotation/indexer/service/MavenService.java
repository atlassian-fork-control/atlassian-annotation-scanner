package com.atlassian.annotation.indexer.service;

import com.atlassian.annotation.indexer.model.atr.TestGav;

import java.io.FileNotFoundException;
import java.io.InputStream;


public interface MavenService {

    InputStream getMavenArtifact(TestGav testGav) throws FileNotFoundException;

}
