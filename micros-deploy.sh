# usage ./micros-deply.sh [ddev,adev,stg-east]
# requires npm, mvn, docker
#

npm install -g @atlassian/micros-cli
mvn clean install
docker build -t docker.atlassian.io/atlassian/atlassian-annotation-scanner:latest .
docker push docker.atlassian.io/atlassian/atlassian-annotation-scanner:latest

# https://extranet.atlassian.com/display/MICROS/How-to%3A+Use+Micros+Environments 
micros service:deploy atlassian-annotation-scanner -e ${1:-ddev} 
